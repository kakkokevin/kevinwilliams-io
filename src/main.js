(() => {
    let sections = [];

    document.addEventListener('DOMContentLoaded', () => {
        sections = Array.from(document.getElementsByTagName('section'));
        updatePage();
    });

    window.addEventListener('hashchange', () => {
        updatePage();
    });

    function updatePage() {
        let hash = location.hash.replace('#', '');
        // If there's no hash, we're implicitly on the front page
        if (hash === '') {
            sections[0].classList.add('active-page');
            return;
        }
        // Else, set active the correct page, and inactivate the others
        for (let s of sections) {
            s.classList.remove('active-page');
            if (s.id === hash) {
                s.classList.add('active-page');
            }
        }
    }
})();